import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:news_app/src/models/category_model.dart';
import 'package:news_app/src/models/news_models.dart';

import 'package:http/http.dart' as http;

class NewsService with ChangeNotifier {
  final String _baseUrl = 'newsapi.org';
  final String _apikey = '9db717dea13e4774a1fede810c97e5ac';

  List<Article> headlines = [];
  String _selectedCategory = 'business';

  bool _isLoading = true;

  List<Category> categories = [
    Category(FontAwesomeIcons.building, 'business'),
    Category(FontAwesomeIcons.tv, 'entertainment'),
    Category(FontAwesomeIcons.addressCard, 'general'),
    Category(FontAwesomeIcons.headSideVirus, 'health'),
    Category(FontAwesomeIcons.vials, 'science'),
    Category(FontAwesomeIcons.volleyballBall, 'sports'),
    Category(FontAwesomeIcons.memory, 'technology'),
  ];

  Map<String, List<Article>> categoryArticles = {};

  NewsService() {
    getTopHeadlines();

    for (var element in categories) {
      categoryArticles[element.name] = [];
    }   
    getArticlesByCategory(_selectedCategory);
    
  }

  get selectedCategory => _selectedCategory;

  set setSelectedCategory(String valor) {
    _selectedCategory = valor;
    
    _isLoading = true;
    getArticlesByCategory(valor);
    notifyListeners();
  }

  bool get isLoading => _isLoading;
  
  List<Article>? get getArticulosCategoriaSeleccionada => categoryArticles[selectedCategory];

  getTopHeadlines() async {
    final url = Uri.https(
        _baseUrl, 'v2/top-headlines', {'apiKey': _apikey, 'country': 'ca'});

    final response = await http.get(url);
    final newsResponse = newsResponseFromJson(response.body);

    headlines.addAll(newsResponse.articles);

    notifyListeners();
  }

  getArticlesByCategory(String category) async {

    if (categoryArticles[category]!.isNotEmpty) {
      _isLoading = false;
      notifyListeners();
      return categoryArticles[category];
    }

    final url = Uri.https(_baseUrl, 'v2/top-headlines',
        {'apiKey': _apikey, 'country': 'ca', 'category': category});

    final response = await http.get(url);
    final newsResponse = newsResponseFromJson(response.body);

    categoryArticles[category]?.addAll(newsResponse.articles);
    _isLoading = false;
    notifyListeners();
  }
}
